#!/usr/bin/env python
# -*- coding: utf-8 -*-

import fontforge
import glob
import sys
import lxml.etree as ET
FontName = sys.argv[1]
SVG_DIR = glob.glob('output-svg/'+FontName+'/over/*.svg')
font = fontforge.open()
compositeChar = [192, 193, 194, 195, 196, 199, 200, 201, 202, 203, 204, 205, 206, 207, 210, 211, 212, 213, 214, 217, 218, 219, 220, 224, 225, 226, 227, 231, 232, 233, 234, 235, 236, 237, 238, 239, 242, 243, 244, 249, 250, 251, 252, 350, 351]

for glyph in SVG_DIR:
    with open(glyph, 'rt') as f:
        treeLet = ET.parse(f)
    rootLet = treeLet.getroot()
    chasse = rootLet.get('width')

    letter = glyph.split("/")[-1].replace(".svg", "")
    print('dec : ' + letter + '| width : ' + chasse)
    svg_file = open(glyph, "r")

    letter_char = font.createChar(int(letter))
    letter_char.importOutlines(glyph)
    letter_char.width = int(chasse)
    # letter_char.reverseDirection()
    # letter_char.correctDirection()

    for letter_comp in compositeChar:
        # print(letter_comp)
        glyphAcc = font.createChar(letter_comp)
        # glyphAcc.width = 16
        glyphAcc.build()
       # glyphAcc.left_side_bearing = glyphAcc.right_side_bearing = (glyphAcc.left_side_bearing + glyphAcc.right_side_bearing)/2

espace = font.createChar(32)
espace.width = font['f'].width
espacefine = font.createChar(8201)
espacefine.width = int(font['f'].width / 3)
font.fontname = FontName
font.familyname = 'fontName'
font.generate('FINAL/' + FontName + '.otf')
